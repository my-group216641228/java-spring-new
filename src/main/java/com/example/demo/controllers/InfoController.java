package com.example.demo.controllers;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Enity.BookEntity;
import com.example.demo.Model.ResResult;
import com.example.demo.modelVM.SearchVM;
import com.example.demo.repositories.BookRepository;
import com.example.demo.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController()
@RequestMapping("api/info")
@Api(tags = "User API", description = "APIs for managing users")
public class InfoController {
    @Autowired
    private BookRepository repository;
    @Autowired
    private BookService bookService;

    Logger logger = LoggerFactory.getLogger(InfoController.class);

    @Operation(summary = "Get")
    @GetMapping("hello")
    public ResResult list(SearchVM request) {
        return bookService.findPaging(request);
    }

    @GetMapping("GetByID")
    public ResResult GetByID(@RequestParam UUID id) {

        return bookService.findByID(id);
    }

    @GetMapping("book")
    @Operation(summary = "get book", description = "Get user information based on the provided ID")
    public ResResult book(@RequestParam(required = false) Boolean status) {
        List<BookEntity> list;
        if (status != null) {
            list = repository.findByStatus(status);
        } else {
            list = repository.findAll();
        }
        // list.stream().
        return ResResult.to400Result(list);
    }

    @PostMapping("add")
    @Operation(summary = "Add book")
    public ResResult postMethodName(@RequestBody BookEntity entity) {
        return bookService.addBook(entity);
    }

    @PutMapping("Update")
    public ResResult putMethodName(@RequestBody BookEntity entity) {

        try {
            return bookService.updateBook(entity);
        } catch (Exception e) {
            return ResResult.toResult(500, e.getMessage());
        }
    }

    @DeleteMapping
    public ResResult delete(@RequestParam UUID id) {

        return bookService.deleteBook(id);
    }
}
