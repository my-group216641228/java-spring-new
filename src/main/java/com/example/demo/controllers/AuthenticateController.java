package com.example.demo.controllers;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.RegisterDTO;
import com.example.demo.Enity.UserEntity;
import com.example.demo.Model.ResResult;
import com.example.demo.component.MyComponent;
import com.example.demo.config.BusExcep;
import com.example.demo.modelVM.SearchVM;
import com.example.demo.service.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("api/auth")
@RequiredArgsConstructor
public class AuthenticateController extends ResResult {

    private final UserService _userService;
    private final MyComponent _myComponent;

    @GetMapping("hello")
    public ResResult list(SearchVM request) {
        _myComponent.hello();
        try {
            int t = 4;
            // System.out.println(t / 0);
            if (t % 2 == 0) {
                throw new BusExcep("null");
            }
        } catch (Exception e) {
            boolean c = e instanceof BusExcep;
            throw new RuntimeException(e.getMessage());
        }
        return Ok(request);
    }

    @GetMapping
    public ResResult getALL() {

        return Ok(_userService.getAll());
    }

    @GetMapping("paging")
    public ResResult getPaging(@Valid SearchVM vm) {

        return Ok(_userService.getPaging(vm));
    }

    @GetMapping("demo")
    public ResponseEntity<UserEntity> demo(@RequestParam int i) {

        return ResponseEntity.ok(_userService.Add(i));
    }

    @PostMapping("regiter")
    public ResponseEntity<UserEntity> regiter(@Valid @RequestBody RegisterDTO inp) {
        var rs = _userService.Register(inp);
        return ResponseEntity.ok(rs);
    }

    @GetMapping("getCustomDataNative")
    public ResponseEntity<?> getCustomDataNative() {

        return ResponseEntity.ok(_userService.getCustomDataNative());
    }

    @GetMapping("getCustomData")
    public ResponseEntity<?> getCustomData() {

        return ResponseEntity.ok(_userService.getCustomData());
    }

    @GetMapping("getByID")
    public ResponseEntity<?> getByID(@RequestParam UUID uid) {

        return ResponseEntity.ok(ResResult.Ok(_userService.getByID(uid)));
    }
}
