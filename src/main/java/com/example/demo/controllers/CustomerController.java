package com.example.demo.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.Book;
import com.example.demo.Enity.BookEntity;
import com.example.demo.Model.ResResult;
import com.example.demo.component.MyComponent;
import com.example.demo.service.BookService;
import com.example.demo.service.RedisService;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("api/customer")
public class CustomerController {
    private MyComponent myComponent;
    private RedisService redisService;
    private BookService bookService;

    // @Autowired
    public CustomerController(MyComponent myComponent, RedisService redisService, BookService bookService,
            UserService _UserService) {
        this.myComponent = myComponent;
        this.redisService = redisService;
        this.bookService = bookService;
    }

    @GetMapping("demo")
    public ResResult demo() {
        LocalDate date = LocalDate.now();
        System.out.println(date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        System.out.println(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        myComponent.hello();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("bien1", "dádđ");
        map.put("bien2", 2000);

        return ResResult.Ok(map);
    }

    @GetMapping
    public ResResult get(HttpServletRequest request) {

        System.out.println(request.getHeader(null));
        Book bk = new Book();

        List<Book> bList = new ArrayList<Book>();

        bList.add(new Book(UUID.randomUUID(), "mot", true));
        bList.add(new Book(UUID.randomUUID(), "hai", true));

        bk.setBookID(UUID.randomUUID());
        bk.setTitle("khong co gì");

        try {
            ObjectMapper oMapper = new ObjectMapper();

            System.out.println();

            Object value = redisService.getValue2("book");
            if (value != null) {
                return ResResult.Ok(value);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        myComponent.hello();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("bien1", "dádđ");
        map.put("bien2", 2000);
        ObjectMapper mapper = new ObjectMapper();
        try {
            var dta = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ResResult.Ok(map);
    }

    @GetMapping("mapper")
    public ResResult Mapper() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("bien1", "dádđ");
        map.put("bien2", 2000);
        map.put("bien3", "xin chào");
        map.put("Today", LocalDate.now());
        String data = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            data = mapper.writeValueAsString(map);
            var obj = mapper.readValue(data, new TypeReference<Map<String, Object>>() {
            });
            var d = (LocalDate) obj.get("Today");
            System.out.println(d.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResResult.Ok(data);
    }

    @GetMapping("setkey")
    public ResResult setkey() {

        List<Book> bList = new ArrayList<Book>();

        bList.add(new Book(UUID.randomUUID(), "mot", true));
        bList.add(new Book(UUID.randomUUID(), "hai", true));

        try {
            // ObjectMapper oMapper = new ObjectMapper();
            // String json = oMapper.writeValueAsString(bList);
            // System.out.println();

            // redisService.setValue("book", json);

            redisService.setValue("book", bList);
        } catch (Exception e) {
            System.out.println(e);
        }
        return ResResult.Ok("set key success");
    }

    @PostMapping
    public ResResult deleteKey() {
        try {
            redisService.deleteKeyPattern("bo");
            return ResResult.Ok("Xóa key thành công");
        } catch (Exception e) {
            return ResResult.toResult(500, e.getMessage());
        }
    }

    @GetMapping(value = "getAsync")
    public ResResult getAsync(@RequestParam String param) {
        CompletableFuture<List<BookEntity>> b1 = bookService.findAllAsync();
        CompletableFuture<List<BookEntity>> b2 = bookService.findAllAsync();
        CompletableFuture<List<BookEntity>> b3 = bookService.findAllAsync();
        System.out.println();
        return ResResult.Ok("Ok");
    }

}
