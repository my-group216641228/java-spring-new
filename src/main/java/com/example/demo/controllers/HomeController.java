package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @GetMapping("/{id}")
    public String Home(@RequestParam() String name, @PathVariable(required = false) int id) {
        return "Hello World " + name + " " + id;
    }
}
