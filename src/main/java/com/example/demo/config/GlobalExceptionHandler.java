package com.example.demo.config;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.demo.Model.ResResult;

@ControllerAdvice
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResResult handleException(Exception ex) {
        StringBuilder msg = new StringBuilder();
        msg.append("[Message]: " + ex.getMessage());
        msg.append(" [File]: " + ex.getStackTrace()[0].getFileName());
        msg.append(" [Line]: " + ex.getStackTrace()[0].getLineNumber());
        return ResResult.toResult(500, msg.toString());
    }

    @ExceptionHandler(BusExcep.class)
    public ResResult validationBusException(BusExcep ex) {

        return ResResult.toResult(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResResult validationNotValidException(MethodArgumentNotValidException ex) {
        var error = ex.getBindingResult().getAllErrors().get(0);
        String fieldName = ((FieldError) error).getField();
        // String fieldName2 = error.get;
        return ResResult.to400Result(fieldName + " " + error.getDefaultMessage());
    }
}
