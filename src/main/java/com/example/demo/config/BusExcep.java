package com.example.demo.config;

import lombok.Getter;
import lombok.Setter;

/**
 * Lop Exception loi nghiep vu, validate
 */
@Getter
@Setter
public class BusExcep extends RuntimeException {
    private int code;

    public BusExcep(String msg) {
        super(msg);
    }

    public static BusExcep NotFound(String msg) {
        BusExcep bus = new BusExcep(msg);
        bus.setCode(404);
        return bus;
    }
}
