package com.example.demo.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ResResult {
    private boolean isOk = true;
    private int code;
    private int statusCode = 200;
    private Object object;

    public void setStatusCode(int code) {
        statusCode = code;
        this.isOk = code >= 200 && code < 300;
    }

    public static ResResult Ok(Object obj) {
        ResResult result = new ResResult();
        result.setObject(obj);

        return result;
    }

    public static ResResult to400Result(Object obj) {
        ResResult result = new ResResult();
        result.setObject(obj);
        result.setStatusCode(400);
        return result;
    }

    public static ResResult NotFound() {
        ResResult result = new ResResult();
        result.setObject("NOT FOUND");
        result.setStatusCode(404);
        return result;
    }

    public static ResResult toResult(int statusCode, Object obj) {
        ResResult result = new ResResult();
        result.setObject(obj);
        result.setStatusCode(statusCode);
        return result;
    }
}
