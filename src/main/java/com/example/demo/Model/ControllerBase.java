package com.example.demo.Model;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerBase {
    public static ResponseEntity<ResResult> Ok(ResResult obj) {

        return ResponseEntity.status(HttpStatus.OK).body(obj);
    }
}
