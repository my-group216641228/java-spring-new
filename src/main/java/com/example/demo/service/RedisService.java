package com.example.demo.service;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RedisService {
    private final RedisTemplate<String, String> redisTemplate;
    private ObjectMapper oMapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(RedisService.class);

    public RedisService(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
        oMapper.findAndRegisterModules();
    }

    public void setValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value, 1, TimeUnit.MINUTES);
    }

    public <T> void setValue(String key, T value) {
        // try {

        String valueStr;
        try {
            valueStr = oMapper.writeValueAsString(value);
            redisTemplate.opsForValue().set(key, valueStr, 1, TimeUnit.MINUTES);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public <T> void setValue(String key, T value, int timeout, TimeUnit timeUnit) {
        try {
            var valueStr = oMapper.writeValueAsString(value);
            redisTemplate.opsForValue().set(key, valueStr, timeout, timeUnit);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public String getValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public Object getValue2(String key) {
        try {
            String value = redisTemplate.opsForValue().get(key);
            if (value == null)
                return null;
            return oMapper.readValue(value, Object.class);
        } catch (Exception e) {
            // System.out.println("ERORR GET: " + e.getMessage());
            logger.error(e.getMessage());
            return null;
        }
    }

    public void deleteKey(String key) {
        redisTemplate.delete(key);
    }

    /**
     * Xóa nhiều key khớp với patern : text *
     * 
     * @param pattern dạng : text*
     */
    public void deleteKeyPattern(String pattern) {
        Set<String> matchingKeys = redisTemplate.keys(pattern + "*");

        redisTemplate.delete(matchingKeys);
    }

    /**
     * Kiểm tra xem key có tồn tại
     * 
     * @param key
     * @return
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
