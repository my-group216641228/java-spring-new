package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.demo.Enity.BookEntity;
import com.example.demo.Model.ResResult;
import com.example.demo.config.BusExcep;
import com.example.demo.modelVM.SearchVM;
import com.example.demo.repositories.BookRepository;

@Service
public class BookService {
    private Logger logger = LoggerFactory.getLogger(BookService.class);

    private final String R_KEY = "BOOK:";
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private RedisService redisService;

    @Async
    public CompletableFuture<List<BookEntity>> findAllAsync() {
        logger.info("get list by {}", Thread.currentThread().getName());
        Page<BookEntity> page = bookRepository.findAll(PageRequest.of(0, 10));

        return CompletableFuture.completedFuture(page.getContent());
    }

    public ResResult findPaging(SearchVM req) {
        String key = R_KEY;

        // if (p % 2 == 0) {
        // throw new ValidationException("Validate Excep");
        // }
        // if (redisService.hasKey(key)) {
        // return ResResult.Ok(redisService.getValue2(key));
        // }
        var pageReq = PageRequest.of(req.getPage(), req.getPageSize(), Sort.by(Sort.Order.desc("createAt")));
        var page = bookRepository.findByTitleContaining(req.getSearch(), pageReq);

        // List<BookEntity> data = page.getContent().stream().map(b -> {

        Map<String, Object> kq = new HashMap<>();
        kq.put("getTotalPages", page.getTotalPages());
        kq.put("getNumber", page.getNumber());
        kq.put("total", page.getTotalElements());
        kq.put("data", page.getContent());

        // redisService.setValue(key, kq);

        // logger.info("--- Cache", "get");
        return ResResult.Ok(kq);
    }

    public ResResult findByID(UUID id) {
        String key = R_KEY + "ID" + id;
        if (redisService.hasKey(key)) {
            return ResResult.Ok(redisService.getValue2(key));
        }
        Optional<BookEntity> entity = bookRepository.findById(id.toString());
        if (entity.isPresent()) {
            redisService.setValue(key, entity.get());
            return ResResult.Ok(entity.get());
        }
        return ResResult.NotFound();
    }

    public ResResult addBook(BookEntity data) {
        var num = bookRepository.countByTitle(data.getTitle());
        System.out.println(num);
        if (num > 0) {
            throw new BusExcep("Tên đã tồn tại");
        }

        // entity.setBookID(UUID.randomUUID().toString());
        // var uid = UUID.randomUUID();
        data.setCreateAt(LocalDateTime.now());
        data.setLastUpdate(LocalDateTime.now());

        var result = bookRepository.saveAndFlush(data);
        System.out.println(result);
        redisService.deleteKeyPattern(R_KEY);
        return ResResult.Ok("Thêm thành công");
    }

    public ResResult updateBook(BookEntity entity) {
        // try {
        // var book = bookRepository.findById(entity.getBookID());
        // if (!book.isPresent())
        // return ResResult.to404Result();
        // var title = bookRepository.findByTitle(entity.getTitle());
        // if (title != null && title.getBookID() != book.get().getBookID())
        // return ResResult.to400Result("Tên đã tồn tại");

        // // entity.setBookID(UUID.randomUUID().toString());
        // BookEntity bookUpdate = book.get();
        // bookUpdate.setTitle(entity.getTitle());
        // bookUpdate.setStatus(entity.getStatus());
        // bookUpdate.setLastUpdate(LocalDateTime.now());
        // bookRepository.saveAndFlush(bookUpdate);

        // redisService.deleteKeyPattern(R_KEY);
        // return ResResult.toResultOk("Update thành công");
        // } catch (Exception e) {
        // return ResResult.to400Result(e.getMessage());
        // }

        return ResResult.Ok("Update thành công");
    }

    public ResResult deleteBook(UUID id) {

        try {
            Optional<BookEntity> result = bookRepository.findById(id.toString());

            if (!result.isPresent())
                return ResResult.to400Result("Không có data");

            bookRepository.delete(result.get());
            redisService.deleteKeyPattern(R_KEY);
            return ResResult.Ok("Xóa thành công");
        } catch (Exception e) {
            return ResResult.to400Result(e.getMessage());
        }
    }
}
