package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.tomcat.util.bcel.classfile.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.demo.DTO.RegisterDTO;
import com.example.demo.DTO.UserDTO;
import com.example.demo.Enity.UserEntity;
import com.example.demo.common.ConstantMsg;
import com.example.demo.config.BusExcep;
import com.example.demo.modelVM.SearchVM;
import com.example.demo.repositories.UserRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository _userRepository;

    public List<UserEntity> getAll() {
        return _userRepository.findAll();
    }

    public UserEntity getByID(UUID uid) {

        Optional<UserEntity> user = _userRepository.findById(uid.toString());
        // if (!user.isPresent()) {
        // throw BusExcep.NotFound(ConstantMsg.NOT_FOUND);
        // }
        UserEntity u = user.orElseThrow(() -> BusExcep.NotFound(ConstantMsg.NOT_FOUND));

        return u;
    }

    public Map<String, Object> getPaging(SearchVM vm) {
        var pageRequet = PageRequest.of(vm.getPage(), vm.getPageSize());// , Sort.by(Sort.Order.desc("CreateDate"))

        Page<UserEntity> page = _userRepository.findByUserNameContainingOrderByCreateDateDesc(vm.getSearch(),
                pageRequet);

        Map<String, Object> kq = new HashMap<>();
        kq.put("total", page.getTotalElements());
        kq.put("data", page.getContent());
        return kq;
    }

    @Transactional()
    public UserEntity Register(RegisterDTO dta) {

        // UserEntity u = new UserEntity();
        // u.setUserName(dta.getUserName());
        // var checkExist = _userRepository.exists(Example.of(u));
        // System.out.println(checkExist);

        var check = _userRepository.countByUserName(dta.getUserName());
        if (check > 0) {
            throw new BusExcep("UserName Exist");
        }
        UserEntity user = UserEntity.builder()
                .userName(dta.getUserName())
                .passWord(dta.getPassWord()).build();

        var rs = _userRepository.saveAndFlush(user);
        return rs;
    }

    @Transactional
    public UserEntity Add(int i) {
        UserEntity u = UserEntity.builder()
                .userName("hihihi")
                .build();
        if (i % 2 == 0) {
            throw new BusExcep("Loi roi hihihih");
        }
        return _userRepository.saveAndFlush(u);
    }

    public List<UserEntity> getCustomDataNative() {
        return _userRepository.getCustomDataNative();
    }

    public List<UserDTO> getCustomData() {
        return _userRepository.getCustomData();
    }
}
