package com.example.demo.component;

import org.springframework.stereotype.Component;

@Component
public class MyComponent {
    public void hello() {
        System.out.println("This msg from component");
    }
}
