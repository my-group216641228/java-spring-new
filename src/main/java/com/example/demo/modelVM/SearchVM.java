package com.example.demo.modelVM;

import jakarta.validation.constraints.Min;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SearchVM {
    @Min(value = 1) // , message = "phải lớn hơn 1"
    private int PageSize = 20;
    @Min(value = 0)
    private int Page = 0;
    private String search = "";
}
