package com.example.demo.Enity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.*;

@Entity
@Table(name = "user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
@JsonPropertyOrder(alphabetic = false)
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String UserID;
    // @Column(name = "UserName")
    private String userName;
    private String email;
    private String passWord;
    private String address;
    private String fullName;
    private boolean isDelete;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createDate;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime lastUpdate;
    private float height;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate Dob;

    @PrePersist
    public void setDateTime() {
        this.createDate = LocalDateTime.now();
        this.lastUpdate = LocalDateTime.now();
    }
}
