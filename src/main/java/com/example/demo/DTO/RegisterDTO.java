package com.example.demo.DTO;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDTO {
    @NotNull(message = "UserName not null")
    @NotBlank(message = "UserName not blank")
    @NotEmpty(message = "UserName not empty")
    private String UserName;
    @NotBlank(message = "PassWord not blank") //
    @NotNull(message = "Password not null")
    @NotEmpty() // message = "PassWord not empty"
    @Size(min = 6, max = 20)
    private String PassWord;
}
