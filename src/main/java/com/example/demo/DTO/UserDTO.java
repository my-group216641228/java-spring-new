package com.example.demo.DTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private String UserID;
    private String UserName;
    private String FullName;
    private String Email;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate Dob;
}
