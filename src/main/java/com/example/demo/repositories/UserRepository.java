package com.example.demo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.DTO.UserDTO;
import com.example.demo.Enity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, String> {
    Optional<UserEntity> findByUserName(String UserName);

    @Query("SELECT NEW com.example.demo.DTO.UserDTO(u.UserID, u.userName, u.email, u.fullName, u.Dob)" +
            " FROM UserEntity u ")
    List<UserDTO> getCustomData();

    @Query(value = "SELECT u.UserID, u.userName, u.address, u.email, u.fullName, u.Dob" +
            " FROM `user` u ", nativeQuery = true)
    List<UserEntity> getCustomDataNative();

    int countByUserName(String UserName);

    Page<UserEntity> findByUserNameContainingOrderByCreateDateDesc(String UserName, Pageable pageable);
}
