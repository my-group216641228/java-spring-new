package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Enity.BookEntity;

public interface BookRepository extends JpaRepository<BookEntity, String> {
    // Iterable<Book> findByStatus(boolean status);
    List<BookEntity> findByStatus(boolean Status);

    long countByTitle(String title);

    BookEntity findByTitle(String title);

    Page<BookEntity> findByTitleContaining(String title, Pageable pageable);
}
